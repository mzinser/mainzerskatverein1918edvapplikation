package com.example.mainzerskatverein1918edvapplikation.game_model

enum class SpielFarbe private constructor(private val displayName: String, val value: Int) {
    KARO("Karo", 9),
    HERZ("Herz", 10),
    PIK("Pik", 11),
    KREUZ("Kreuz", 12),
    GRAND("Grand", 24);

    override fun toString(): String {
        return displayName
    }

}
