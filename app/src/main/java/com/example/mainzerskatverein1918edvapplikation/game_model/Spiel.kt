package com.example.mainzerskatverein1918edvapplikation.game_model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Spiel(
        var typ: SpielTyp = SpielTyp.FARBE,
        var farbe: SpielFarbe = SpielFarbe.KARO,
        var buben: Buben = Buben.OHNE_VIER,
        var hand: Boolean = false,
        var ouvertRevolution: OuvertRevolution = OuvertRevolution.NONE,
        var gewonnen: Boolean = true
        ) : Parcelable {

    // Ansagen
    var kontraRe = KontraRe.NONE

    var schneiderSchwarz = SchneiderSchwarz.NONE

    var bock = false
    var durchmarsch: Boolean = false
    var jungfrau: Boolean = false
    var schieber: Int = 0
    var spaltArsch: Boolean = false
    var ramschwert: Int = 0

    private fun calculateFarbMultiplicator(): Int {

        var multiplicator = 1 + buben.value
        if (hand) multiplicator += 1
        multiplicator += ouvertRevolution.value
        multiplicator += kontraRe.value
        if (bock) multiplicator += 1

        if (schneiderSchwarz == SchneiderSchwarz.SCHNEIDER) {
            multiplicator += 1
        }
        if (schneiderSchwarz == SchneiderSchwarz.SCHNEIDER_ANGESAGT) {
            multiplicator += 2
        }
        if (schneiderSchwarz == SchneiderSchwarz.SCHNEIDER_SCHWARZ) {
            multiplicator += 3
        }
        if (schneiderSchwarz == SchneiderSchwarz.SCHNEIDER_SCHWARZ_ANGESAGT) {
            multiplicator += 4
        }

        return multiplicator

    }

    fun calculateScore(): Int {
        var score = 0
        when (typ) {
            SpielTyp.FARBE -> {
                val multiplicator = calculateFarbMultiplicator()
                score = multiplicator * farbe.value
            }
            SpielTyp.NULL -> {
            }
            SpielTyp.RAMSCH -> {
            }
        }

        if(!gewonnen) score *= 2

        return score
    }

}
