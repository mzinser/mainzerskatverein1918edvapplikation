package com.example.mainzerskatverein1918edvapplikation.items

import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.example.mainzerskatverein1918edvapplikation.models.GameNight
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.gamenight_listitem.view.*
import java.text.SimpleDateFormat

class GameNightItem(val gameNight: GameNight): Item<GroupieViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.gamenight_listitem
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy")
        val date = simpleDateFormat.format(gameNight.data.date)
        viewHolder.itemView.date_gamenight.text = date
        viewHolder.itemView.n_games_gamenight.text = "Anzahl Spiele: ${gameNight.data.games.size}"
        var sumPoints: Int = 0
        gameNight.data.games.values.forEach {
            sumPoints += it.points
        }
        viewHolder.itemView.points_gamenight.text = "Anzahl Punkte: $sumPoints"
        var playerString: String = "Spieler: "
        gameNight.data.player_uids.forEach {
            val curPlayer = DataStorage.playersMap[it]
            playerString += "${curPlayer!!.playerData.name}, "
        }
        playerString = playerString.removeSuffix(", ")
        viewHolder.itemView.players_gamenight.text = playerString
    }

}
