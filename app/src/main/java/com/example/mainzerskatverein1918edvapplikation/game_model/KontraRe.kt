package com.example.mainzerskatverein1918edvapplikation.game_model

enum class KontraRe private constructor(private val displayName: String, val value: Int) {

    NONE("Kein", 0),
    KONTRA("Kontra", 1),
    KONTRA_RE("Re", 2);

    override fun toString(): String {
        return displayName
    }

}
