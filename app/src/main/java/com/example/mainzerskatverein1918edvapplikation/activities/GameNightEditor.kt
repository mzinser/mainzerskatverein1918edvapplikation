package com.example.mainzerskatverein1918edvapplikation.activities

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.widget.CheckBox
import android.widget.DatePicker
import androidx.appcompat.app.AppCompatActivity
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.example.mainzerskatverein1918edvapplikation.models.GameData
import com.example.mainzerskatverein1918edvapplikation.models.GameNightData
import kotlinx.android.synthetic.main.activity_game_night_editor.*
import java.util.*
import kotlin.collections.ArrayList


class GameNightEditor : AppCompatActivity() {

    private var currentGameNightData: GameNightData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_night_editor)
        supportActionBar?.title = "Neuen Abend erstellen"

        addPlayers()

        val gameNightData = intent.getParcelableExtra<GameNightData>(SeasonActivity.GAMENIGHT_KEY)
        if (gameNightData != null) {
            currentGameNightData = gameNightData
            location_game_night_editor.setText(gameNightData.location)
            val cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"))
            cal.time = gameNightData.date
            val year = cal[Calendar.YEAR]
            val month = cal[Calendar.MONTH]
            val day = cal[Calendar.DAY_OF_MONTH]
            date_game_night_editor.updateDate(year, month, day)
            setCheckedPlayers(gameNightData.player_uids)
        } else {
            val id = UUID.randomUUID().toString()
            currentGameNightData = GameNightData(id,
                    location_game_night_editor.text.toString(),
                    date_game_night_editor.getDate(),
                    ArrayList<String>(),
                    HashMap<String, GameData>())
        }

        ok_button_game_night_editor.setOnClickListener {
            val playerList = getCheckedPlayers()
            if(checkPlayerCount(playerList)) {
                currentGameNightData!!.location = location_game_night_editor.text.toString()
                currentGameNightData!!.date = date_game_night_editor.getDate()
                currentGameNightData!!.player_uids = playerList
                intent.putExtra(SeasonActivity.GAMENIGHT_KEY, currentGameNightData)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }

    private fun DatePicker.getDate(): Date {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        return calendar.time
    }

    private fun checkPlayerCount(playerList: ArrayList<String>): Boolean {
        if(playerList.size < 3) {
            val alert = AlertDialog.Builder(this)
            alert.setTitle("Zu geringe Spieleranzahl")
            alert.setMessage("Es sollten mindestens drei Spieler an einem Abend teilnehmen!")
            alert.setPositiveButton("OK", null)
            alert.show()
        } else{
            return true
        }
        return false
    }

    private fun getCheckedPlayers(): ArrayList<String> {
        val playerList = ArrayList<String>()
        for (x in 0 until layout_scrollview_game_night_editor.childCount){
            val cb = layout_scrollview_game_night_editor.getChildAt(x) as CheckBox
            if(cb.isChecked) playerList.add(cb.tag.toString())
        }
        return playerList
    }

    private fun setCheckedPlayers(playerList: ArrayList<String>) {
        for (x in 0 until layout_scrollview_game_night_editor.childCount){
            val cb = layout_scrollview_game_night_editor.getChildAt(x) as CheckBox
            if(cb.tag in playerList) {
                cb.isChecked = true
            }
        }
    }

    private fun addPlayers() {
        DataStorage.playersMap.values.forEach {
            val cb = CheckBox(applicationContext)
            cb.textSize = 18f
            cb.tag = it.playerData.uid
            cb.text = it.playerData.name
            layout_scrollview_game_night_editor.addView(cb)
        }
    }
}
