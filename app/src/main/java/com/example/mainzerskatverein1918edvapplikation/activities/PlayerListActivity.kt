package com.example.mainzerskatverein1918edvapplikation.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.items.PlayerItem
import com.example.mainzerskatverein1918edvapplikation.items.SelfPlayerItem
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.google.firebase.auth.FirebaseAuth
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_player_list.*

class PlayerListActivity : AppCompatActivity() {

    private val adapter = GroupAdapter<GroupieViewHolder>()

    companion object {
        const val PLAYER_KEY = "PLAYER_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MyLogger", "PlayerList::onCreate")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_list)
        supportActionBar?.title = "Aktuelle Spieler"

        initAdapter()
        refreshRecyclerView()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("MyLogger", "PlayerList::onActivityResult")

        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 0 && resultCode == Activity.RESULT_OK) {
            Log.d("MyLogger", "PlayerList::onActivityResult: Result ok")
            refreshRecyclerView()
        }

    }

    private fun initAdapter() {
        Log.d("MyLogger", "PlayerList::initAdapter")

        recycler_players.adapter = adapter
        recycler_players.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
        adapter.setOnItemClickListener { item, view ->
            if(item !is SelfPlayerItem) {
                return@setOnItemClickListener
            }
            if(item.player.playerData.uid != FirebaseAuth.getInstance().uid){
                return@setOnItemClickListener
            }
            Log.d("MyLogger:PlayerList", "Opening player editor")
            val intent = Intent(view.context, RegisterActivity::class.java)
            intent.putExtra(PLAYER_KEY, item.player.playerData)
            startActivityForResult(intent, 0)
        }

    }

    private fun refreshRecyclerView() {
        Log.d("MyLogger", "PlayerList::refreshRecyclerView")

        adapter.clear()
        DataStorage.playersMap.values.forEach {
            if(it.playerData.uid == FirebaseAuth.getInstance().uid) {
                adapter.add(SelfPlayerItem(it))
            } else {
                adapter.add(PlayerItem(it))
            }
        }

    }

}

