package com.example.mainzerskatverein1918edvapplikation.models

import android.os.Parcelable
import android.util.Log
import com.google.firebase.database.*
import kotlinx.android.parcel.Parcelize

class Player(
        val playerData: PlayerData
) {

    var ref: DatabaseReference? = null
    private var initialized: Boolean = false

    init {
        initReference()
    }

    fun initReference() {

        ref = FirebaseDatabase.getInstance().getReference("/players/${playerData.uid}/")

        Log.d("MyLogger", "init: ${ref.toString()}")
        Log.d("MyLogger", "init: ${playerData.uid}")

        ref!!.addChildEventListener(object: ChildEventListener {

            override fun onCancelled(p0: DatabaseError) {
                Log.d("MyLogger", "Player::onCancelled: $p0")
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Player::onChildMoved: $p0")
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Player::onChildChanged: $this")
                if(p0.key.toString() == "name") {
                    playerData.name = p0.value.toString()
                    Log.d("MyLogger", "Player::onChildChanged() name: ${playerData.name}")
                }
                if(p0.key.toString() == "imageURL") {
                    playerData.imageURL = p0.value.toString()
                    Log.d("MyLogger", "Player::onChildChanged() imageURL: ${playerData.imageURL}")
                }
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Player::onChildAdded: $p0")
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                Log.d("MyLogger", "Player::onChildRemoved: $p0")
            }

        })

        initialized = true

    }

}