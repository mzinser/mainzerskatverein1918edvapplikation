package com.example.mainzerskatverein1918edvapplikation.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mainzerskatverein1918edvapplikation.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        Log.d("MyLogger", "LoginActivity: Starting Activity")

        login_button_login.setOnClickListener {
            Log.d("MyLogger", "LoginActivity: User pressed login button")
            loginPlayer()
        }

    }

    private fun loginPlayer() {

        val email = email_text_login.text.toString()
        val password = password_text_login.text.toString()

        if(email.isEmpty() || password.isEmpty()) {
            val toast = Toast.makeText(this, "Bitte Email und Passwort angeben!", Toast.LENGTH_SHORT)
            toast.setMargin(50f, 50f)
            toast.show()
            return
        }

        signInPlayer(email, password)
    }

    private fun signInPlayer(email: String, password: String) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        Log.d("MyLogger", "signInPlayer: Successfully logged in.")
                        goToMainMenu()
                    } else{
                        registerPlayerInFirebase(email, password)
                    }
                }
                .addOnFailureListener {
                    Log.d("MyLogger", "signInPlayer: Something went wrong:" +
                            "uid: ${it.message}")
                    Toast.makeText(this, "${it.message}", Toast.LENGTH_SHORT).show()
                }

    }


    private fun registerPlayerInFirebase(email: String, password: String) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(!it.isSuccessful) return@addOnCompleteListener

                    Log.d("MyLogger", "registerPlayer: User successfully registered:" +
                            "uid: ${it.result!!.user!!.uid}")
                    goToRegisterSceen()
                }
                .addOnFailureListener {
                    Log.d("MyLogger", "registerPlayer: Something went wrong:" +
                            "uid: ${it.message}")
                    Toast.makeText(this, "${it.message}", Toast.LENGTH_SHORT).show()
                }
    }

    private fun goToRegisterSceen() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    private fun goToMainMenu() {
        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
        finish()
    }


}
