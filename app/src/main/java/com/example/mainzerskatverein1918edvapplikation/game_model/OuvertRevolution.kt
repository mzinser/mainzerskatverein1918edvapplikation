package com.example.mainzerskatverein1918edvapplikation.game_model

enum class OuvertRevolution private constructor(private val displayName: String, val value: Int) {

    NONE("Kein", 0),
    OUVERT("Ouvert", 1),
    OUVERT_REVOLUTION("Revolution", 2);

    override fun toString(): String {
        return displayName
    }

}
