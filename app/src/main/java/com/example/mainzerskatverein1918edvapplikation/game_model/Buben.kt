package com.example.mainzerskatverein1918edvapplikation.game_model

enum class Buben private constructor(private val displayName: String, val value: Int) {

    MIT_EINEM("Mit einem", 1),
    OHNE_EINEN("Ohne einen", 1),
    MIT_ZWEI("Mit zwei", 2),
    OHNE_ZWEI("Ohne zwei", 2),
    MIT_DREI("Mit drei", 3),
    OHNE_DREI("Ohne drei", 3),
    MIT_VIER("Mit vier", 4),
    OHNE_VIER("Ohne vier", 4);

    override fun toString(): String {
        return displayName
    }

}
