package com.example.mainzerskatverein1918edvapplikation.models

import android.util.Log
import com.google.firebase.database.*

class Game (
    val data: GameData,
    val seasonId: String,
    val gameNightId: String
) {

    private var ref: DatabaseReference? = null
    private var initialized: Boolean = false

    init {
        initReference()
    }

    private fun initReference() {
        Log.d("MyLogger", "Game::initReference")

        ref = FirebaseDatabase.getInstance().getReference("/seasons/${seasonId}/gameNights/${gameNightId}/games/${data.id}")
        ref!!.addChildEventListener(object: ChildEventListener {

            override fun onCancelled(p0: DatabaseError) {
                Log.d("MyLogger", "Game::onCancelled: $p0")
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Game::onChildMoved: $p0")
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Game::onChildChanged: $this")
                //if(p0.key.toString() == "name") {
                //    seasonData.name = p0.value.toString()
                //    Log.d("MyLogger", "GameNight::onChildChanged() name: ${seasonData.name}")
                //}
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Game::onChildAdded: $p0")
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                Log.d("MyLogger", "Game::onChildRemoved: $p0")
            }

        })

        initialized = true

    }

    fun saveToFirebase() {
        Log.d("MyLogger", "Game::saveToFirebase(): $this")
        ref?.setValue(data)
                ?.addOnSuccessListener {
                    Log.d("MyLogger", "Game::saveToFirebase(): Season saved in FirebaseDatabase: $this")
                }
                ?.addOnFailureListener {
                    Log.d("MyLogger", "Game::saveToFirebase(): Something went wrong: ${it.message}")
                }
    }

}