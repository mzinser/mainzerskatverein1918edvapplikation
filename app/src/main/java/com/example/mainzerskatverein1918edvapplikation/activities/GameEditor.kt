package com.example.mainzerskatverein1918edvapplikation.activities

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.*
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.game_model.Buben
import com.example.mainzerskatverein1918edvapplikation.game_model.OuvertRevolution
import com.example.mainzerskatverein1918edvapplikation.game_model.Spiel
import com.example.mainzerskatverein1918edvapplikation.game_model.SpielFarbe

class GameEditor
//public Dialog d;
//public Button yes, no;

(var c: Context)// TODO Auto-generated constructor stub
    : Dialog(c), android.view.View.OnClickListener, android.widget.AdapterView.OnItemSelectedListener {

    private var farbeSpinner: Spinner? = null
    private var bubenSpinner: Spinner? = null

    private var handCheckBox: CheckBox? = null
    private var ouvertCheckBox: CheckBox? = null
    private var revolutionCheckBox: CheckBox? = null

    private var kontraCheckBox: CheckBox? = null
    private var reCheckBox: CheckBox? = null

    private var schneiderCheckBox: CheckBox? = null
    private var schwarzCheckBox: CheckBox? = null
    private var bockCheckBox: CheckBox? = null
    private var schneiderAngesagtCheckBox: CheckBox? = null
    private var schwarzAngesagtCheckBox: CheckBox? = null

    private var ergebnisText: TextView? = null

    private var spiel = Spiel()

    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.game_editor)

        ergebnisText = findViewById<View>(R.id.ergebnis_anzeige_text) as TextView

        farbeSpinner = findViewById<View>(R.id.farbe_spinner) as Spinner
        farbeSpinner!!.adapter = ArrayAdapter(this.c, android.R.layout.simple_spinner_item, SpielFarbe.values())
        farbeSpinner!!.onItemSelectedListener = this

        bubenSpinner = findViewById<View>(R.id.buben_spinner) as Spinner
        bubenSpinner!!.adapter = ArrayAdapter(this.c, android.R.layout.simple_spinner_item, Buben.values())
        bubenSpinner!!.onItemSelectedListener = this

        handCheckBox = findViewById<View>(R.id.spiel_hand_checkbox) as CheckBox
        handCheckBox!!.setOnClickListener(this)


        ouvertCheckBox = findViewById<View>(R.id.spiel_ouvert_checkbox) as CheckBox
        ouvertCheckBox!!.setOnClickListener(this)

        revolutionCheckBox = findViewById<View>(R.id.spiel_revolution_checkbox) as CheckBox
        revolutionCheckBox!!.setOnClickListener(this)

        kontraCheckBox = findViewById<View>(R.id.ansagen_kontra_checkbox) as CheckBox
        kontraCheckBox!!.setOnClickListener(this)

        reCheckBox = findViewById<View>(R.id.ansagen_re_checkbox) as CheckBox
        reCheckBox!!.setOnClickListener(this)

        schneiderCheckBox = findViewById<View>(R.id.schneiderschwarz_schneider_checkbox) as CheckBox
        schneiderCheckBox!!.setOnClickListener(this)

        schwarzCheckBox = findViewById<View>(R.id.schneiderschwarz_schwarz_checkbox) as CheckBox
        schwarzCheckBox!!.setOnClickListener(this)

        bockCheckBox = findViewById<View>(R.id.schneiderschwarz_bock_checkbox) as CheckBox
        bockCheckBox!!.setOnClickListener(this)

        schneiderAngesagtCheckBox = findViewById<View>(R.id.schneiderschwarz_schneider_angesagt_checkBox) as CheckBox
        schneiderAngesagtCheckBox!!.setOnClickListener(this)

        schwarzAngesagtCheckBox = findViewById<View>(R.id.schneiderschwarz_schwarz_angesagt_checkBox) as CheckBox
        schwarzAngesagtCheckBox!!.setOnClickListener(this)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.spiel_revolution_checkbox -> if (revolutionCheckBox!!.isChecked) {
                ouvertCheckBox!!.isChecked = true
                ouvertCheckBox!!.isEnabled = false
            } else {
                ouvertCheckBox!!.isEnabled = true
            }
            R.id.ansagen_re_checkbox -> if (reCheckBox!!.isChecked) {
                kontraCheckBox!!.isChecked = true
                kontraCheckBox!!.isEnabled = false
            } else {
                kontraCheckBox!!.isEnabled = true
            }
            R.id.schneiderschwarz_schwarz_checkbox -> if (schwarzCheckBox!!.isChecked) {
                schneiderCheckBox!!.isChecked = true
                schneiderCheckBox!!.isEnabled = false
            } else {
                schneiderCheckBox!!.isEnabled = true
            }
            R.id.schneiderschwarz_schwarz_angesagt_checkBox -> if (schwarzAngesagtCheckBox!!.isChecked) {
                schneiderAngesagtCheckBox!!.isChecked = true
                schneiderAngesagtCheckBox!!.isEnabled = false
            } else {
                schneiderAngesagtCheckBox!!.isEnabled = true
            }
            else -> {
            }
        }
        spiel = updateSpiel(spiel)
        ergebnisText!!.text = Integer.toString(spiel.calculateScore())
    }

    private fun updateSpiel(spiel: Spiel): Spiel {

        val result = spiel

        result.farbe = farbeSpinner!!.selectedItem as SpielFarbe
        result.buben = bubenSpinner!!.selectedItem as Buben
        result.hand = handCheckBox!!.isChecked

        if (ouvertCheckBox!!.isChecked && !revolutionCheckBox!!.isChecked) {
            result.ouvertRevolution = OuvertRevolution.OUVERT
        } else if (ouvertCheckBox!!.isChecked && revolutionCheckBox!!.isChecked) {
            result.ouvertRevolution = OuvertRevolution.OUVERT_REVOLUTION
        } else if (!ouvertCheckBox!!.isChecked && !revolutionCheckBox!!.isChecked) {
            result.ouvertRevolution = OuvertRevolution.NONE
        }

        return result

    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        spiel = updateSpiel(spiel)
        ergebnisText!!.text = Integer.toString(spiel.calculateScore())
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }
}