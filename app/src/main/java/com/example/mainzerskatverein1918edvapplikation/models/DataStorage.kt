package com.example.mainzerskatverein1918edvapplikation.models

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DataStorage {

    companion object {

        val playersMap = HashMap<String, Player>()
        val seasonsMap = HashMap<String, Season>()

        fun fetchPlayers() {
            Log.d("MyLogger", "DataStorage::fetchPlayers()")
            val ref = FirebaseDatabase.getInstance().getReference("/players")
            ref.addListenerForSingleValueEvent(object: ValueEventListener {

                override fun onCancelled(p0: DatabaseError) {
                    Log.d("MyLogger", "Error in fetchPlayers(): Something went wrong: $p0")
                }

                override fun onDataChange(p0: DataSnapshot) {
                    Log.d("MyLogger", "DataStorage::fetchPlayers(): Updating players")
                    p0.children.forEach{
                        val playerData = it.getValue(PlayerData::class.java)
                        if(playerData != null){
                            Log.d("MyLogger", "Found player: ${it.toString()}")
                            playersMap[playerData.uid] = Player(playerData)
                        }
                    }
                }

            })

        }

        fun fetchSeasons() {
            Log.d("MyLogger", "DataStorage::fetchSeasons()")
            val ref = FirebaseDatabase.getInstance().getReference("/seasons")
            ref.addListenerForSingleValueEvent(object: ValueEventListener {

                override fun onCancelled(p0: DatabaseError) {
                    Log.e("MyLogger", "DataStorage::fetchSeasons(): Something went wrong: $p0")
                }

                override fun onDataChange(p0: DataSnapshot) {
                    Log.d("MyLogger", "DataStorage::fetchSeasons(): Updating players")
                    p0.children.forEach{
                        val seasonData = it.getValue(SeasonData::class.java)
                        if(seasonData != null){
                            Log.d("MyLogger", "DataStorage::fetchSeasons(): Found season ${it.toString()}")
                            seasonsMap[seasonData.id] = Season(seasonData)
                        }
                    }
                }

            })

        }

        fun addSeason() {
            val id = UUID.randomUUID().toString()
            val name = "Season " + DataStorage.seasonsMap.size
            val seasonData = SeasonData(id, name, HashMap<String, GameNightData>())
            val curSeason = Season(seasonData)
            seasonsMap[id] = curSeason
            curSeason.saveToFirebase()
        }

        fun getNameFromUid(uid: String): String {
            val player = playersMap[uid]
            var name: String
            if(player == null){
                name = "Player not found"
            } else{
                name = player.playerData.name
            }
            return name
        }

        fun getNamesFromUids(uids: ArrayList<String>): ArrayList<String> {
            val result = ArrayList<String>()
            uids.forEach {
                result.add(getNameFromUid(it))
            }
            return result
        }

    }

}