package com.example.mainzerskatverein1918edvapplikation.items

import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.models.Player
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.selfplayer_listitem.view.*

class SelfPlayerItem(val player: Player): Item<GroupieViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.selfplayer_listitem
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.player_name.text = player.playerData.name
        Picasso.get()
                .load(player.playerData.imageURL)
                .resize(200, 200)
                .centerCrop()
                .into(viewHolder.itemView.image)
    }
    
}
