package com.example.mainzerskatverein1918edvapplikation.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MyLogger", "SettingsActivity::onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        initLayout()

        logout_textview_settings.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            goToLogin()
        }

        image_button_settings.setOnClickListener {
            Log.d("MyLogger", "RegisterActivity: Image button pressed")
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

    }

    private fun initLayout() {
        Log.d("MyLogger", "SettingsActivity::initLayout")
        val curUser = FirebaseAuth.getInstance().currentUser
        if(curUser == null) {
            Log.e("MyLogger", "SettingsActivity::initLayout: curUser null!")
            return
        }
        val curPlayer = DataStorage.playersMap[curUser.uid]
        if(curPlayer == null) {
            Log.e("MyLogger", "SettingsActivity::initLayout: curPlayer null!")
            return
        }

        name_textview_settings.text = curPlayer.playerData.name
        email_textview_settings.text = curUser.email

        Picasso.get().load(curPlayer.playerData.imageURL).into(select_imageview_settings)
        image_button_settings.alpha = 0f

    }

    private fun goToLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

}
