package com.example.mainzerskatverein1918edvapplikation.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Parcelize
class GameNightData(
        val id: String,
        var location: String,
        var date: Date,
        var player_uids: ArrayList<String>,
        val games: HashMap<String, GameData>
): Parcelable {
    constructor() : this("", "", Date(), ArrayList<String>(), HashMap<String, GameData>())
}