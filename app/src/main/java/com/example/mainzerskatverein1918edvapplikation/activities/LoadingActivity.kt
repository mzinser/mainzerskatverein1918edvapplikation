package com.example.mainzerskatverein1918edvapplikation.activities

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.example.mainzerskatverein1918edvapplikation.models.Player
import com.example.mainzerskatverein1918edvapplikation.models.PlayerData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class LoadingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MyLogger", "LoadingActivity::onCreate")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)

        DataStorage.fetchPlayers()
        DataStorage.fetchSeasons()
        verifiyUserIsLoggedIn()

    }

    private fun goToLogin() {
        Log.d("MyLogger", "LoadingActivity::goToLogin")

        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()

    }

    private fun goToMenu() {
        Log.d("MyLogger", "LoadingActivity::goToMenu")

        val intent = Intent(this, MenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()

    }

    private fun verifiyUserIsLoggedIn() {
        Log.d("MyLogger", "LoadingActivity::verifiyUserIsLoggedIn")

        val uid = FirebaseAuth.getInstance().uid
        if(uid == null) {
            goToLogin()
        } else{
            goToMenu()
        }

    }

}
