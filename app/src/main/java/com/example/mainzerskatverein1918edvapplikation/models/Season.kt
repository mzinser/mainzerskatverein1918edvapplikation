package com.example.mainzerskatverein1918edvapplikation.models

import android.os.Parcelable
import android.util.Log
import com.example.mainzerskatverein1918edvapplikation.activities.SeasonListActivity
import com.google.firebase.database.*
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class Season(
        val seasonData: SeasonData
) {

    private var ref: DatabaseReference? = null
    val gameNights: HashMap<String, GameNight> = HashMap<String, GameNight>()
    private var initialized: Boolean = false

    init {
        initReference()
        seasonData.gameNights.values.forEach {
            gameNights[it.id] = GameNight(it, seasonData.id)
        }
    }

    private fun initReference() {
        Log.d("MyLogger", "Season::initReference")

        ref = FirebaseDatabase.getInstance().getReference("/seasons/${seasonData.id}/")
        ref!!.addChildEventListener(object: ChildEventListener {

            override fun onCancelled(p0: DatabaseError) {
                Log.d("MyLogger", "Season::onCancelled: $p0")
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Season::onChildMoved: $p0")
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Season::onChildChanged: $this")
                if(p0.key.toString() == "name") {
                    seasonData.name = p0.value.toString()
                    Log.d("MyLogger", "Season::onChildChanged() name: ${seasonData.name}")
                }
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "Season::onChildAdded: $p0")
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                Log.d("MyLogger", "Season::onChildRemoved: $p0")
            }

        })

        initialized = true

    }

    fun saveToFirebase() {
        Log.d("MyLogger", "Season::saveToFirebase(): $this")
        ref?.setValue(seasonData)
                ?.addOnSuccessListener {
                    Log.d("MyLogger", "Season::saveToFirebase(): Season saved in FirebaseDatabase: $this")
                }
                ?.addOnFailureListener {
                    Log.d("MyLogger", "Season::saveToFirebase(): Something went wrong: ${it.message}")
                }
    }

    fun addGameNight(gameNightData: GameNightData) {
        val curGameNight = GameNight(gameNightData, seasonData.id)
        curGameNight.saveToFirebase()
        gameNights[gameNightData.id] = curGameNight
        SeasonListActivity.adapter.notifyDataSetChanged()
    }

}