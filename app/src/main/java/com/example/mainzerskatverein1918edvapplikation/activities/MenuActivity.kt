package com.example.mainzerskatverein1918edvapplikation.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MyLogger", "MenuActivity::onCreate")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        player_button.setOnClickListener {
            openPlayerMenu()
        }

        seasons_button_menu.setOnClickListener {
            val intent = Intent(this, SeasonListActivity::class.java)
            startActivity(intent)
        }

        fab.setOnClickListener { view ->
            //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            //        .setAction("Action", null).show()
            //val gameEditor = GameEditor(view.context)
            //gameEditor.show()
            //val intent = Intent(this, GameActivity::class.java)
            //startActivity(intent)
        }

    }

    private fun openPlayerMenu() {
        val intent = Intent(this, PlayerListActivity::class.java)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_settings -> {
                Log.d("MyLogger", "MenuActivity::onOptionsItemSelected: Opening player editor")
                val curUid = FirebaseAuth.getInstance().uid
                val curPlayer = DataStorage.playersMap[curUid]
                if(curPlayer != null) {
                    val intent = Intent(this, SettingsActivity::class.java)
                    intent.putExtra(PlayerListActivity.PLAYER_KEY, curPlayer.playerData)
                    startActivityForResult(intent, 0)
                }

            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

}
