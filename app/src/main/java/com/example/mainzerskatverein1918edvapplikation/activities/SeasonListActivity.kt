package com.example.mainzerskatverein1918edvapplikation.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.items.SeasonItem
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_seasonlist.*

class SeasonListActivity : AppCompatActivity() {

    companion object {
        val adapter = GroupAdapter<GroupieViewHolder>()
        const val SEASON_KEY = "SEASON_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MyLogger", "SeasonListActivity::onCreate")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seasonlist)
        supportActionBar?.title = "Aktuelle Saisons"

        initAdapter()
        refreshRecyclerView()

        add_season_button_seasons.setOnClickListener {
            DataStorage.addSeason()
            refreshRecyclerView()
        }

    }

    private fun initAdapter() {
        Log.d("MyLogger", "SeasonListActivity::initAdapter")
        recyclerview_seasons.adapter = adapter
        recyclerview_seasons.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
        adapter.setOnItemClickListener { item, view ->
            Log.d("MyLogger", "SeasonListActivity:setOnItemClickListener")
            val intent = Intent(view.context, SeasonActivity::class.java)
            val curItem = item as SeasonItem
            intent.putExtra(SEASON_KEY, curItem.season.seasonData)
            startActivityForResult(intent, 0)
        }
    }

    private fun refreshRecyclerView() {
        Log.d("MyLogger", "SeasonListActivity::refreshRecyclerView")

        adapter.clear()
        DataStorage.seasonsMap.values.forEach {
            Log.d("MyLogger", "SeasonListActivity::refreshRecyclerView: Adding season $it")
            adapter.add(SeasonItem(it))
        }

    }


}
