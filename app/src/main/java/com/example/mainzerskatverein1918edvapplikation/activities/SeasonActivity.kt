package com.example.mainzerskatverein1918edvapplikation.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.items.GameNightItem
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.example.mainzerskatverein1918edvapplikation.models.GameNightData
import com.example.mainzerskatverein1918edvapplikation.models.Season
import com.example.mainzerskatverein1918edvapplikation.models.SeasonData
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_season.*

class SeasonActivity : AppCompatActivity() {

    private var curSeason: Season? = null

    companion object {
        val adapter = GroupAdapter<GroupieViewHolder>()
        const val GAMENIGHT_KEY = "GAMENIGHT_KEY"
        const val SEASON_ID_KEY = "SEASON_ID_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MyLogger", "SeasonActivity::onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_season)

        val season = intent.getParcelableExtra<SeasonData>(SeasonListActivity.SEASON_KEY)
        if (season != null) {
            supportActionBar?.title = season.name
            curSeason = DataStorage.seasonsMap[season.id]
        } else {
            Log.e("MyLogger", "SeasonActivity::onCreate: Season is null!")
            finish()
        }

        initAdapter()
        refreshRecyclerView()

        add_gamenight_button_season.setOnClickListener {
            Log.d("MyLogger", "SeasonActivity: Adding Game Night")
            val intent = Intent(this, GameNightEditor::class.java)
            startActivityForResult(intent, 0)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("MyLogger", "SeasonActivity::onActivityResult")

        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            Log.d("MyLogger", "GameNightActivity::onActivityResult: Result ok")
            val gameNightData = data.getParcelableExtra<GameNightData>(GAMENIGHT_KEY)
            curSeason!!.addGameNight(gameNightData)
            refreshRecyclerView()
        }

    }

    private fun initAdapter() {
        Log.d("MyLogger", "SeasonActivity::initAdapter")
        recyclerview_gamenights.adapter = adapter
        recyclerview_gamenights.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
        adapter.setOnItemClickListener { item, view ->
            Log.d("MyLogger", "SeasonActivity:setOnItemClickListener")
            val intent = Intent(view.context, GameNightActivity::class.java)
            val curItem = item as GameNightItem
            intent.putExtra(SEASON_ID_KEY, curSeason!!.seasonData.id)
            intent.putExtra(GAMENIGHT_KEY, curItem.gameNight.data)
            startActivityForResult(intent, 0)
        }
    }

    private fun refreshRecyclerView() {
        Log.d("MyLogger", "SeasonActivity::refreshRecyclerView")

        adapter.clear()
        val result = curSeason!!.gameNights.toList().sortedByDescending { (_, value) -> value.data.date}.toMap()
        result.values.forEach {
            Log.d("MyLogger", "SeasonActivity::refreshRecyclerView: Adding game night $it")
            adapter.add(GameNightItem(it))
        }

    }
}
