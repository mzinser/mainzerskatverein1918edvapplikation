package com.example.mainzerskatverein1918edvapplikation.items

import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.game_model.KontraRe
import com.example.mainzerskatverein1918edvapplikation.game_model.SchneiderSchwarz
import com.example.mainzerskatverein1918edvapplikation.game_model.SpielFarbe
import com.example.mainzerskatverein1918edvapplikation.game_model.SpielTyp
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.example.mainzerskatverein1918edvapplikation.models.Game
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.game_listitem.view.*
import kotlinx.android.synthetic.main.gamenight_listitem.view.*
import java.text.SimpleDateFormat

class GameItem(val game: Game): Item<GroupieViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.game_listitem
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.player_game.text = "Spieler: ${DataStorage.getNameFromUid(game.data.playerUid)},"
        if(game.data.player_wins) {
            viewHolder.itemView.winorloose.text = "gewonnen"
            viewHolder.itemView.winorloose.setTextColor(Color.GREEN)
        }else{
            viewHolder.itemView.winorloose.text = "verloren"
            viewHolder.itemView.winorloose.setTextColor(Color.RED)
        }

        val looserString = getLooserString(game.data.looserUids)
        viewHolder.itemView.n_points_game.text = "${game.data.points} Punkte für $looserString"

        var iconPath: Int
        var bubenText: String = ""
        when(game.data.game.typ) {
            SpielTyp.FARBE -> {
                when(game.data.game.farbe) {
                    SpielFarbe.KARO -> iconPath = R.drawable.karo
                    SpielFarbe.HERZ -> iconPath = R.drawable.herz
                    SpielFarbe.PIK -> iconPath = R.drawable.pik
                    SpielFarbe.KREUZ -> iconPath = R.drawable.kreuz
                    else -> { iconPath = 0 }
                }
                bubenText += game.data.game.typ.toString()
                bubenText += ", " + game.data.game.buben.toString()
            }
            else -> { iconPath = 0 }
        }

        if(game.data.game.hand) {
            bubenText += ", Hand"
        }
        if(game.data.game.schneiderSchwarz != SchneiderSchwarz.NONE) {
            bubenText += ", " + game.data.game.schneiderSchwarz.toString()
        }
        if(game.data.game.kontraRe != KontraRe.NONE) {
            bubenText += ", " + game.data.game.kontraRe.toString()
        }

        viewHolder.itemView.date_game.text = bubenText
        viewHolder.itemView.image.setImageResource(iconPath)
    }

    private fun getLooserString(looserUids: ArrayList<String>) : String {
        val looserNames = DataStorage.getNamesFromUids(looserUids)
        return looserNames.toString().removePrefix("[").removeSuffix("]")
    }

}
