package com.example.mainzerskatverein1918edvapplikation.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.game_model.Spiel
import com.example.mainzerskatverein1918edvapplikation.items.GameItem
import com.example.mainzerskatverein1918edvapplikation.models.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.activity_game_night.*
import kotlinx.android.synthetic.main.activity_season.*
import java.text.SimpleDateFormat

class GameNightActivity : AppCompatActivity() {


    private var curGameNight: GameNight? = null
    private val adapter = GroupAdapter<GroupieViewHolder>()

    companion object {
        const val PLAYERS_KEY = "PLAYERS_KEY"
        const val GAME_KEY = "GAME_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MyLogger", "GameNightActivity::onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_night)

        val gameNight = intent.getParcelableExtra<GameNightData>(SeasonActivity.GAMENIGHT_KEY)
        val seasonId = intent.getStringExtra(SeasonActivity.SEASON_ID_KEY)

        if (gameNight != null) {
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy")
            val date = simpleDateFormat.format(gameNight.date)
            supportActionBar?.title = date
            curGameNight = DataStorage.seasonsMap[seasonId]!!.gameNights[gameNight.id]
        } else {
            Log.e("MyLogger", "GameNightActivity::onCreate: GameNight is null!")
            finish()
        }

        initAdapter()
        refreshRecyclerView()

        add_game_button_gamenight.setOnClickListener {
            Log.d("MyLogger", "GameNightActivity: Adding Game")
            val intent = Intent(this, GameActivity::class.java)
            intent.putStringArrayListExtra(PLAYERS_KEY, curGameNight?.data!!.player_uids)
            startActivityForResult(intent, 0)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("MyLogger", "GameNightActivity::onActivityResult")

        super.onActivityResult(requestCode, resultCode, data)
        // coming from GameEditor
        if(requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            Log.d("MyLogger", "GameNightActivity::onActivityResult: Getting result from GameActivity")
            val gameData = data.getParcelableExtra<GameData>(GAME_KEY)
            curGameNight!!.addGame(gameData)
        }
        // coming from GameNightEditor
        if(requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            Log.d("MyLogger", "GameNightActivity::onActivityResult: Getting result from GameNightEditor")
            val gameData = data.getParcelableExtra<GameNightData>(SeasonActivity.GAMENIGHT_KEY)
            curGameNight!!.updateData(gameData)
            curGameNight!!.saveToFirebase()
        }
        refreshRecyclerView()
    }

    private fun initAdapter() {
        Log.d("MyLogger", "GameNightActivity::initAdapter")
        recyclerview_games.adapter = adapter
        recyclerview_games.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
        adapter.setOnItemClickListener { item, view ->
            Log.d("MyLogger", "GameNightActivity:setOnItemClickListener")
            //val intent = Intent(view.context, SeasonActivity::class.java)
            //val curItem = item as SeasonItem
            //intent.putExtra(SEASON_KEY, curItem.season.seasonData)
            //startActivityForResult(intent, 0)
        }
    }

    private fun refreshRecyclerView() {
        Log.d("MyLogger", "GameNightActivity::refreshRecyclerView")

        adapter.clear()
        curGameNight!!.games.values.forEach {
            Log.d("MyLogger", "GameNightActivity::refreshRecyclerView: Adding game $it")
            adapter.add(GameItem(it))
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_edit -> {
                Log.d("MyLogger", "GameNightActivity::onOptionsItemSelected: Opening GameNight editor")
                val intent = Intent(this, GameNightEditor::class.java)
                intent.putExtra(SeasonActivity.GAMENIGHT_KEY, curGameNight!!.data)
                startActivityForResult(intent, 1)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_game_night, menu)
        return super.onCreateOptionsMenu(menu)
    }


}
