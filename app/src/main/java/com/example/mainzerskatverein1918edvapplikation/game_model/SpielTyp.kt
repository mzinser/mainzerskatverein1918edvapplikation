package com.example.mainzerskatverein1918edvapplikation.game_model

enum class SpielTyp private constructor(private val displayName: String) {

    FARBE("Karo"),
    NULL("Herz"),
    RAMSCH("Pik");

    override fun toString(): String {
        return displayName
    }
}
