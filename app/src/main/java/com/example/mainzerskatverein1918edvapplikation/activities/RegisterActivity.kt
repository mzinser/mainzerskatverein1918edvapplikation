package com.example.mainzerskatverein1918edvapplikation.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.example.mainzerskatverein1918edvapplikation.models.Player
import com.example.mainzerskatverein1918edvapplikation.models.PlayerData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_register.*
import java.io.ByteArrayOutputStream
import java.util.*


class RegisterActivity : AppCompatActivity() {

    private var comingFromLogin: Boolean = true
    private var selectedPhotoURI: Uri? = null
    private var currentPlayer: Player? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        Log.d("MyLogger", "RegisterActivity: Starting RegisterActivity")

        val player = intent.getParcelableExtra<PlayerData>(PlayerListActivity.PLAYER_KEY)
        if (player != null) {
            comingFromLogin = false
            name_text_register.setText(player.name)
            Picasso.get().load(player.imageURL).into(select_imageview_register)
            image_button_register.alpha = 0f
            currentPlayer = DataStorage.playersMap[player.uid]
        } else {
            val uid = FirebaseAuth.getInstance().uid
            val playerData = PlayerData(uid!!, "", "")
            currentPlayer = Player(playerData)
        }

        image_button_register.setOnClickListener {
            Log.d("MyLogger", "RegisterActivity: Image button pressed")
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        ok_button_register.setOnClickListener {
            if(currentPlayer != null) {
                if(selectedPhotoURI != null) {
                    uploadImageToFirebaseStorage(currentPlayer!!)
                } else {
                    savePlayerToFirebaseDatabase(currentPlayer!!)
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("MyLogger", "onActivityResult")
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            Log.d("MyLogger", "Photo was selected")
            selectedPhotoURI = data.data
            Picasso.get().load(selectedPhotoURI).into(select_imageview_register)
            image_button_register.alpha = 0f
        }
    }

    private fun goToMainMenu() {
        val intent = Intent(this, MenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    private fun goToPlayerList() {
        intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun uploadImageToFirebaseStorage(player: Player) {
        Log.d("MyLogger", "uploadImageToFirebaseStorage(): $selectedPhotoURI")

        val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoURI)
        val scaleRatio = bitmap.width/bitmap.height
        val resized = Bitmap.createScaledBitmap(bitmap, 200*scaleRatio, 200, true)
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos)
        val data: ByteArray = baos.toByteArray()

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        ref.putBytes(data)
            .addOnSuccessListener {
                Log.d("MyLogger", "Photo uploaded to FirebaseStorage: ${it.metadata?.path}")
                ref.downloadUrl.addOnSuccessListener {
                    player.playerData.imageURL = it.toString()
                    savePlayerToFirebaseDatabase(player)
                }
            }
    }

    private fun savePlayerToFirebaseDatabase(player: Player) {
        Log.d("MyLogger", "savePlayerToFirebaseDatabase(): $player")
        Log.d("MyLogger", "${DataStorage.playersMap.values}")
        Log.d("MyLogger", "commingFromLogin: $comingFromLogin")

        player.playerData.name = name_text_register.text.toString()
        if(comingFromLogin) {
            DataStorage.playersMap[player.playerData.uid] = player
        }

        Log.d("MyLogger", "${player.ref}")

        player.ref?.setValue(player.playerData)
                ?.addOnSuccessListener {
                    Log.d("MyLogger", "Player saved in FirebaseDatabase: $player")
                    Log.d("MyLogger", "${DataStorage.playersMap.values}")
                    if(comingFromLogin) {
                        goToMainMenu()
                    } else {
                        goToPlayerList()
                    }
                }
                ?.addOnFailureListener {
                    Log.d("MyLogger", "savePlayerToFirebaseDatabase something went wrong: ${it.message}")
                }

    }

}

