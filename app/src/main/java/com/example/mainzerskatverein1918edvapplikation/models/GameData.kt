package com.example.mainzerskatverein1918edvapplikation.models

import android.os.Parcelable
import com.example.mainzerskatverein1918edvapplikation.game_model.Spiel
import com.example.mainzerskatverein1918edvapplikation.game_model.SpielFarbe
import com.example.mainzerskatverein1918edvapplikation.game_model.SpielTyp
import kotlinx.android.parcel.Parcelize


@Parcelize
class GameData(
        val id: String,
        val playerUid: String,
        var points: Int,
        var looserUids: ArrayList<String>,
        var game: Spiel,
        var player_wins: Boolean
): Parcelable {
    constructor() : this("", "",42, ArrayList<String>(), Spiel(), true)
}