package com.example.mainzerskatverein1918edvapplikation.game_model

enum class SchneiderSchwarz private constructor(private val displayName: String) {

    NONE("Kein"),
    SCHNEIDER("Schneider"),
    SCHNEIDER_ANGESAGT("Schneider angesagt"),
    SCHNEIDER_SCHWARZ("Schwarz"),
    SCHNEIDER_SCHWARZ_ANGESAGT("Schwarz angesagt");

    override fun toString(): String {
        return displayName
    }

}
