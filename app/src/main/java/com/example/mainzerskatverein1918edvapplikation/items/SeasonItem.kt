package com.example.mainzerskatverein1918edvapplikation.items

import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.models.Season
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import kotlinx.android.synthetic.main.season_listitem.view.*

class SeasonItem(val season: Season): Item<GroupieViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.season_listitem
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.season_name.text = season.seasonData.name
        val nNights = season.seasonData.gameNights.size
        viewHolder.itemView.season_n_gamenights.text = "Anzahl Abende: $nNights"
    }

}
