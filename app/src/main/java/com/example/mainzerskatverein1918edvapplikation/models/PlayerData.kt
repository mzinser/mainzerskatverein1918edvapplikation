package com.example.mainzerskatverein1918edvapplikation.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class PlayerData(
        val uid: String,
        var name: String,
        var imageURL: String
): Parcelable {
    constructor() : this("", "", "")
}