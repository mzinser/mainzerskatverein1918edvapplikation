package com.example.mainzerskatverein1918edvapplikation.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.mainzerskatverein1918edvapplikation.R
import com.example.mainzerskatverein1918edvapplikation.game_model.*
import com.example.mainzerskatverein1918edvapplikation.models.DataStorage
import com.example.mainzerskatverein1918edvapplikation.models.GameData
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.game_editor.ansagen_kontra_checkbox
import kotlinx.android.synthetic.main.game_editor.ansagen_re_checkbox
import kotlinx.android.synthetic.main.game_editor.buben_spinner
import kotlinx.android.synthetic.main.game_editor.ergebnis_anzeige_text
import kotlinx.android.synthetic.main.game_editor.farbe_spinner
import kotlinx.android.synthetic.main.game_editor.schneiderschwarz_schneider_angesagt_checkBox
import kotlinx.android.synthetic.main.game_editor.schneiderschwarz_schneider_checkbox
import kotlinx.android.synthetic.main.game_editor.schneiderschwarz_schwarz_angesagt_checkBox
import kotlinx.android.synthetic.main.game_editor.schneiderschwarz_schwarz_checkbox
import kotlinx.android.synthetic.main.game_editor.spiel_hand_checkbox
import kotlinx.android.synthetic.main.game_editor.spiel_ouvert_checkbox
import kotlinx.android.synthetic.main.game_editor.spiel_revolution_checkbox
import java.util.*
import kotlin.collections.ArrayList

class GameActivity : AppCompatActivity() {

    private var spiel = Spiel()
    private lateinit var playerUids: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MyLogger", "GameActivity::onCreate")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        playerUids = intent.getStringArrayListExtra(GameNightActivity.PLAYERS_KEY)
        Log.d("MyLogger", "GameActivity: Players: $playerUids")

        if(playerUids != null){
            val playerNames = DataStorage.getNamesFromUids(playerUids)
            player_spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, playerNames)
        }

        farbe_spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, SpielFarbe.values())
        buben_spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, Buben.values())

        initClickListener()

        ok_button.setOnClickListener {
            val id = UUID.randomUUID().toString()
            val playerUid = playerUids[player_spinner.selectedItemPosition]
            val playerWins = player_wins.isChecked
            val points = spiel.calculateScore()
            var looserUids = getLooserUids()
            val gameData = GameData(id, playerUid, points, looserUids, spiel, playerWins)
            intent = Intent()
            intent.putExtra(GameNightActivity.GAME_KEY, gameData)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }

    private fun updateSpiel(spiel: Spiel): Spiel {

        val result = spiel

        result.gewonnen = player_wins.isChecked

        result.farbe = farbe_spinner.selectedItem as SpielFarbe
        result.buben = buben_spinner.selectedItem as Buben
        result.hand = spiel_hand_checkbox.isChecked

        if (spiel_ouvert_checkbox!!.isChecked && !spiel_revolution_checkbox!!.isChecked) {
            result.ouvertRevolution = OuvertRevolution.OUVERT
        } else if (spiel_ouvert_checkbox!!.isChecked && spiel_revolution_checkbox!!.isChecked) {
            result.ouvertRevolution = OuvertRevolution.OUVERT_REVOLUTION
        } else if (!spiel_ouvert_checkbox!!.isChecked && !spiel_revolution_checkbox!!.isChecked) {
            result.ouvertRevolution = OuvertRevolution.NONE
        }

        result.kontraRe = KontraRe.NONE
        if(ansagen_kontra_checkbox.isChecked) {
            result.kontraRe = KontraRe.KONTRA
        }
        if(ansagen_re_checkbox.isChecked) {
            result.kontraRe = KontraRe.KONTRA_RE
        }

        result.schneiderSchwarz = SchneiderSchwarz.NONE
        if(schneiderschwarz_schneider_checkbox!!.isChecked) {
            result.schneiderSchwarz = SchneiderSchwarz.SCHNEIDER
        }
        if(schneiderschwarz_schneider_angesagt_checkBox!!.isChecked) {
            result.schneiderSchwarz = SchneiderSchwarz.SCHNEIDER_ANGESAGT
        }
        if(schneiderschwarz_schwarz_checkbox!!.isChecked) {
            result.schneiderSchwarz = SchneiderSchwarz.SCHNEIDER_SCHWARZ
        }
        if(schneiderschwarz_schwarz_angesagt_checkBox!!.isChecked) {
            result.schneiderSchwarz = SchneiderSchwarz.SCHNEIDER_SCHWARZ_ANGESAGT
        }

        return result

    }

    private fun initClickListener() {

        player_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                loosers.text = getLooserString()
            }
        }

        farbe_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                spiel = updateSpiel(spiel)
                ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
            }
        }

        buben_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                spiel = updateSpiel(spiel)
                ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
            }
        }

        player_wins.setOnClickListener {
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
            loosers.text = getLooserString()
        }

        spiel_hand_checkbox.setOnClickListener {
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

        spiel_ouvert_checkbox.setOnClickListener {
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

        spiel_revolution_checkbox.setOnClickListener {
            if (spiel_revolution_checkbox.isChecked) {
                spiel_ouvert_checkbox.isChecked = true
                spiel_ouvert_checkbox.isEnabled = false
            } else {
                spiel_ouvert_checkbox.isEnabled = true
            }
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

        ansagen_kontra_checkbox.setOnClickListener {
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

        ansagen_re_checkbox.setOnClickListener {
            if (ansagen_re_checkbox.isChecked) {
                ansagen_kontra_checkbox.isChecked = true
                ansagen_kontra_checkbox.isEnabled = false
            } else {
                ansagen_kontra_checkbox.isEnabled = true
            }
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

        schneiderschwarz_schneider_checkbox.setOnClickListener {
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

        schneiderschwarz_schneider_angesagt_checkBox.setOnClickListener {
            if (schneiderschwarz_schneider_angesagt_checkBox.isChecked) {
                schneiderschwarz_schneider_checkbox.isChecked = true
                schneiderschwarz_schneider_checkbox.isEnabled = false
            } else {
                schneiderschwarz_schneider_checkbox.isEnabled = true
            }
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

        schneiderschwarz_schwarz_checkbox.setOnClickListener {
            if (schneiderschwarz_schwarz_checkbox.isChecked) {
                schneiderschwarz_schneider_checkbox.isChecked = true
                schneiderschwarz_schneider_checkbox.isEnabled = false
            } else {
                schneiderschwarz_schneider_checkbox.isEnabled = true
            }
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

        schneiderschwarz_schwarz_angesagt_checkBox.setOnClickListener {
            if (schneiderschwarz_schwarz_angesagt_checkBox.isChecked) {
                schneiderschwarz_schwarz_checkbox.isChecked = true
                schneiderschwarz_schwarz_checkbox.isEnabled = false
                schneiderschwarz_schneider_checkbox.isChecked = true
                schneiderschwarz_schneider_checkbox.isEnabled = false

            } else {
                schneiderschwarz_schwarz_checkbox.isEnabled = true
                schneiderschwarz_schneider_checkbox.isEnabled = true
            }
            spiel = updateSpiel(spiel)
            ergebnis_anzeige_text.text = Integer.toString(spiel.calculateScore())
        }

    }

    private fun getLooserUids() : ArrayList<String> {
        var looserUids = ArrayList<String>()
        if(player_wins.isChecked) {
            playerUids.forEach {
                if(it == playerUids[player_spinner.selectedItemPosition]) return@forEach
                looserUids.add(it)
            }
        }else{
            looserUids.add(playerUids[player_spinner.selectedItemPosition])
        }

        return looserUids
    }

    private fun getLooserString() : String {
        val looserNames = DataStorage.getNamesFromUids(getLooserUids())
        return looserNames.toString().removePrefix("[").removeSuffix("]")
    }

}
