package com.example.mainzerskatverein1918edvapplikation.models

import android.os.Parcelable
import android.util.Log
import com.example.mainzerskatverein1918edvapplikation.activities.SeasonActivity
import com.google.firebase.database.*
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class GameNight(
        val data: GameNightData,
        val seasonId: String
) {

    private var ref: DatabaseReference? = null
    val games: HashMap<String, Game> = HashMap<String, Game>()
    private var initialized: Boolean = false

    init {
        initReference()
        val gameNightId = data.id
        data.games.values.forEach {
            games[it.id] = Game(it, seasonId, gameNightId)
        }
    }

    private fun initReference() {
        Log.d("MyLogger", "GameNight::initReference")

        ref = FirebaseDatabase.getInstance().getReference("/seasons/${seasonId}/gameNights/${data.id}")
        ref!!.addChildEventListener(object: ChildEventListener {

            override fun onCancelled(p0: DatabaseError) {
                Log.d("MyLogger", "GameNight::onCancelled: $p0")
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "GameNight::onChildMoved: $p0")
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "GameNight::onChildChanged: $this")
                //if(p0.key.toString() == "name") {
                //    seasonData.name = p0.value.toString()
                //    Log.d("MyLogger", "GameNight::onChildChanged() name: ${seasonData.name}")
                //}
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                Log.d("MyLogger", "GameNight::onChildAdded: $p0")
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                Log.d("MyLogger", "GameNight::onChildRemoved: $p0")
            }

        })

        initialized = true

    }

    fun saveToFirebase() {
        Log.d("MyLogger", "GameNight::saveToFirebase(): $this")
        ref?.setValue(data)
                ?.addOnSuccessListener {
                    Log.d("MyLogger", "GameNight::saveToFirebase(): Season saved in FirebaseDatabase: $this")
                }
                ?.addOnFailureListener {
                    Log.d("MyLogger", "GameNight::saveToFirebase(): Something went wrong: ${it.message}")
                }
    }

    fun addGame(gameData: GameData) {
        val curGame = Game(gameData, seasonId, data.id)
        curGame.saveToFirebase()
        games[gameData.id] = curGame
        SeasonActivity.adapter.notifyDataSetChanged()
    }

    fun updateData(gameNightData: GameNightData) {
        data.player_uids = gameNightData.player_uids
        data.date = gameNightData.date
        data.location = gameNightData.location
        SeasonActivity.adapter.notifyDataSetChanged()
    }

    fun getPlayerNames(): ArrayList<String> {
        val playerNames = ArrayList<String>()
        data.player_uids.forEach {
            val player = DataStorage.playersMap[it]
            playerNames.add(player!!.playerData.name)
        }
        return playerNames
    }

}