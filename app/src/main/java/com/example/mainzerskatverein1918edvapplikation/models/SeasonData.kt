package com.example.mainzerskatverein1918edvapplikation.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class SeasonData(
        val id: String,
        var name: String,
        val gameNights: HashMap<String, GameNightData>
): Parcelable {
    constructor() : this("", "", HashMap<String, GameNightData>())
}